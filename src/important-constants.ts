import {Snowflake} from "discord.js";

export const SNOWFLAKE_USER_LLAMOS: Snowflake = "138318226181324800";
export const SNOWFLAKE_ROLE_GM: Snowflake = "344859708022194176";
export const SNOWFLAKE_ROLE_UMP: Snowflake = "451060214477881355";
export const SNOWFLAKE_SERVER_FAKEBASEBALL_MAIN: Snowflake = "344859525582422016";
export const SNOWFLAKE_CHANNEL_UMP_PINGS: Snowflake = "573242415671017493";
export const SNOWFLAKE_CHANNEL_AB_PINGS: Snowflake = "572841324391170059";
export const SNOWFLAKE_CHANNEL_DRAFT: Snowflake = "593178941137616937";
export const SNOWFLAKE_CHANNEL_OOTC: Snowflake = "597114169514393610";
export const SNOWFLAKE_CHANNEL_NL_CENTRAL: Snowflake = "547143097931268096";
export const SNOWFLAKE_CHANNEL_SCOREBOARD: Snowflake = "586319854290862080";

export const DEVELOPMENT_MODE: boolean = parseInt(process.env.DEVELOPMENT) === 1;

export const REDDIT_ID36_FROM_URL_REGEX = /https:?\/\/.*?\/r\/fakebaseball\/comments\/([0-9a-z]{6,7}).*/i;

export const EMBED_FOOTER = {
    icon_url: "https://cdn.discordapp.com/avatars/138318226181324800/a_ee07f391bf1b75ee4c91bc7878fb838f.png",
    text: "If something seems wrong, please message Llamos#0001.",
};
