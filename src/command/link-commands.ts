import {RichEmbed} from "discord.js";
import {Command} from "./command";

class HandbookCommandClass extends Command {

    hidden(): boolean {
        return true;
    }

    async handleCommand(args: string[]): Promise<string | RichEmbed> {
        return `Here's the current MLR Umpire Handbook!\n\nhttps://docs.google.com/document/d/1XbUMvwiG-7hAPD0ZfPKaqOc0l0rj5a7_oVzTXhwDTWc`;
    }
}

class GameSheetCommandClass extends Command {

    hidden(): boolean {
        return true;
    }

    async handleCommand(args: string[]): Promise<string | RichEmbed> {
        return `Here's the current MLR Game Calculator Spreadsheet!\n\nhttps://docs.google.com/spreadsheets/d/1X7vSMeqthukJeCu7Opo99XQJ4UOMCmCQUhvzvOZRnOM`;
    }
}

class RulebookCommandClass extends Command {

    description(): string {
        return "Provides a link for the current MLR rulebook.";
    }

    async handleCommand(args: string[]): Promise<string | RichEmbed> {
        return `Here's the current MLR Rulebook!\n\nhttps://docs.google.com/document/d/1lrAXwv19StC10uWoJBZLo54r4cgL2J33pwobr7nKHYw`;
    }

}

export const HandbookCommand = new HandbookCommandClass();
export const GameSheetCommand = new GameSheetCommandClass();
export const RulebookCommand = new RulebookCommandClass();
