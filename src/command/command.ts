import {Message, RichEmbed} from "discord.js";

/**
 * This Command class should be extended by a sub-class.
 * It represents one possible command for the bot.
 * Only one instance is created per command,
 * so commands should not have any stateful actions.
 */
export class Command {

    public hidden(): boolean {
        return false;
    }

    public nlCentral(): boolean {
        return false;
    }

    public async handleCommand(args: string[], msg: Message): Promise<string | RichEmbed> {
        return "This command isn't implemented yet.";
    }

    public description(): string {
        return "No description provided.";
    }

}
