import * as Discord from "discord.js";
import {Message, RichEmbed} from "discord.js";
import {getLatestGameThreadsForTeam, getPlayer as getPlayerFromDB} from "../database";
import {Command} from "./command";

/**
 * Gives the user the reddit links of the games their teams are playing in
 */
class MyGamesCommandClass extends Command {

    // Create the string fields of every game for that team
    private static createListOfGamesMessage(games: any[]): string {

        let msg = ``;

        for (let game of games) {
            msg += `${game.away_team} vs. ${game.home_team} : https://reddit.com/r/fakebaseball/comments/${game.id36}/\n`;
        }

        return msg;
    }

    // Creates the title such as "Marlins last 5 games". Used mostly for plural vocabulary
    private static createTitleOfGames(teamName: string, numberOfGames: number): string {

        if (numberOfGames <= 0)
            return ``;
        else if (numberOfGames === 1)
            return `${teamName}'s last game.`;
        else
            return `${teamName}'s last ${numberOfGames} games`;

    }

    description(): string {
        return "Provides links to your team's most recent games.";
    }

    public async handleCommand(args: string[], msg: Message): Promise<string | RichEmbed> {

        // Check the DB for the current user's profile,
        // or tell them to provide a name/claim their account.
        let player = await getPlayerFromDB(msg.author.id);
        if (player === undefined)
            return `I don't know who you are yet. Use .claim to link your Discord!`;

        let searchNum = 1;

        // Clamp number between 1-10 if they input something
        if (args.length > 0) {
            searchNum = parseInt(args[0]);

            if (isNaN(searchNum) || searchNum < 1)
                searchNum = 1;
            else if (searchNum > 10)
                searchNum = 10;
        }

        // Get all of the information about that player's team's games in mlr and milr
        let mlrGames = await getLatestGameThreadsForTeam(player.team, searchNum);
        let milrGames = player.team.milrTeam ? await getLatestGameThreadsForTeam(player.team.milrTeam, searchNum) : undefined;

        if (mlrGames === undefined && milrGames === undefined) {
            return `I couldn't find any games for you.`;
        }

        // Now that we have the game threads, we create the embed fields with all the info
        let fields = [];
        if (mlrGames !== undefined) {
            fields.push({
                inline: false,
                name: MyGamesCommandClass.createTitleOfGames(player.team.name, mlrGames.length),
                value: MyGamesCommandClass.createListOfGamesMessage(mlrGames),
            });
        }
        if (milrGames !== undefined) {
            fields.push({
                inline: false,
                name: MyGamesCommandClass.createTitleOfGames(player.team.milrTeam.name, milrGames.length),
                value: MyGamesCommandClass.createListOfGamesMessage(milrGames),
            });
        }

        // Now create the embed and send it
        return new Discord.RichEmbed({
            title: `${player.name}'s games`,
            color: player.team.color,
            fields,
        });

    }

}

export const MyGamesCommand = new MyGamesCommandClass();
