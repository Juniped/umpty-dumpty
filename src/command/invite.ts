import {Command} from "./command";

/**
 * Generates an invite code to bring the bot into new servers.
 */
class InviteCommandClass extends Command {

    description(): string {
        return "Generates an invite link to add this bot to new servers.";
    }

    async handleCommand(args: string[]): Promise<string> {
        return `https://discordapp.com/api/oauth2/authorize?client_id=568862853415567371&permissions=0&scope=bot`;
    }

}

export const InviteCommand = new InviteCommandClass();
