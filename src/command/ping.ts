import {Message, RichEmbed, Snowflake, TextChannel, User} from "discord.js";
import * as rp from "request-promise";
import {getUserSetting} from "../database";
import {SNOWFLAKE_CHANNEL_AB_PINGS, SNOWFLAKE_SERVER_FAKEBASEBALL_MAIN} from "../important-constants";
import {getPlayer} from "../site-pull";
import {Command} from "./command";

class PendingReaction {
    message: Message;
    pingedUser: string;
    pingedSnowflake: Snowflake;
    srcChannel: TextChannel;
    origAuthor: Snowflake;

    constructor(message: Message, pingedUser: string, pingedSnowflake: Snowflake, srcChannel: TextChannel, origAuthor: string) {
        this.message = message;
        this.pingedUser = pingedUser;
        this.pingedSnowflake = pingedSnowflake;
        this.srcChannel = srcChannel;
        this.origAuthor = origAuthor;
    }
}

/**
 * Notifies a player that it's their turn to bat.
 * Only accessible from the #umpires channel.
 */
class PingCommandClass extends Command {

    pendingReactions: PendingReaction[] = [];

    // Hide from other channels
    hidden(): boolean {
        return true;
    }

    async handleCommand(args: string[], msg: Message): Promise<string | RichEmbed> {
        if (args.length < 1) {
            return `I need a player to ping.`;
        }
        let milrMode = false;
        if (args[0].toLowerCase() === "milr") {
            args = args.slice(1, args.length);
            milrMode = true;
        }
        return this.handlePingCommand(args, msg, milrMode);
    }

    // noinspection JSMethodCanBeStatic
    async handlePingCommand(args: string[], msg: Message, milrMode: boolean): Promise<string | RichEmbed> {
        // Make sure we have a player name to ping
        if (args.length < 1)
            return `I need a player to ping.`;

        // Find the player, or give them the search failure message if we couldn't find one
        let searchQuery = args.join(" ");
        let player = await getPlayer(searchQuery);
        if (typeof player === "string")
            return player;

        // Let the ump know if we couldn't ping them at all
        if (!player.discordSnowflake)
            return `That player isn't Discord Verified, so I can't ping them. :cry:`;

        // Create the base message
        let pingMessage = `<@${player.discordSnowflake}>, you're up to bat${milrMode ? " in MiLR" : ""}!`;

        // Find the game link from the site
        let gameReq = player.team ? {
            uri: `https://redditball.xyz/api/v1/games/active/${player.team.tag}`,
            json: true,
        } : undefined;
        let game;
        try {
            game = gameReq ? await rp(gameReq) : undefined;
        } catch (ignored) {
            game = undefined;
        }

        let gameID36 = game ? game.id36 : undefined;

        pingMessage += gameID36 !== undefined ? `\n\nhttps://reddit.com/r/fakebaseball/comments/${gameID36}/` : ``;

        pingMessage += `\n\nTap the baseball below to let the umpire know you've swung.`;

        let targetChannel =
            msg.guild.id === SNOWFLAKE_SERVER_FAKEBASEBALL_MAIN ? msg.guild.channels.get(SNOWFLAKE_CHANNEL_AB_PINGS) as TextChannel :
                msg.guild.id === "593511953952145418" ? msg.guild.channels.get("596774718510596102") as TextChannel :
                    msg.channel;
        let createdMessage = (await targetChannel.send(pingMessage)) as Message;

        // Add the baseball reaction
        await createdMessage.react("⚾");

        // Store for message for when it's reacted to
        this.pendingReactions.push(new PendingReaction(createdMessage, player.name, player.discordSnowflake, msg.channel as TextChannel, msg.author.id));

        // Let the sender know if it wasn't sent to the same channel
        if (targetChannel.id !== msg.channel.id)
            return `Pinged ${player.name}!`;

        return undefined;
    }

    async handlePingReaction(reactedMsg: Message, user: User) {
        for (let [index, pending] of this.pendingReactions.entries()) {
            if (pending.message.id === reactedMsg.id && pending.pingedSnowflake === user.id) {
                this.pendingReactions.splice(index, 1);
                if (await getUserSetting(pending.origAuthor, "ping.umpoptin", true))
                    await pending.srcChannel.send(`<@${pending.origAuthor}>, ${pending.pingedUser} has swung!`);
                break;
            }
        }
    }

}

export const PingCommand = new PingCommandClass();
