import {Named, SwingStat} from "./SwingStat";

export class Park extends Named {
    id: number;
    name: string;
    factorHR: number;
    factor3B: number;
    factor2B: number;
    factor1B: number;
    factorBB: number;
}

export class Team extends Named {
    id: number;
    name: string;
    tag: string;
    park: number | Park;
    colorDiscord: number;
    colorRoster: number;
    colorRosterBG: number;
    milr: boolean;
}

export class BattingType extends SwingStat {
    id: number;
    shortcode: string;
}

export class PitchingType extends SwingStat {
    id: number;
    shortcode: string;
}

export class PitchingBonus extends SwingStat {
    id: number;
    shortcode: string;
}

export class Player extends Named {
    id: number;
    name: string;
    redditName: string;
    discordSnowflake: string;
    team: Team;
    battingType: BattingType;
    pitchingType: PitchingType;
    pitchingBonus: PitchingBonus;
    rightHanded: boolean;
    positionPrimary: string;
    positionSecondary: string;
    positionTertiary: string;
}
